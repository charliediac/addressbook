<?php

/* contact/index.html.twig */
class __TwigTemplate_d00c513938c6bf6fe67de1c17803bc6d39d73823d3415ca1645eeca9acf091e5 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "contact/index.html.twig", 1);
        $this->blocks = [
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "contact/index.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "contact/index.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_body($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "    <a href=\"/contact/create\">
        <button class=\"add-new-btn\">Add new
        </button>
    </a>
    <form method=\"GET\" action=\"/\">
        <input type=\"text\" name=\"term\">
        <input type=\"submit\" value=\"Search\">
    </form>
    <table class=\"contact-tb\">
        <thead>
        <tr>
            <th>Picture</th>
            <th>First Name</th>
            <th>Last Name</th>
            <th>Birthday</th>
            <th>Phone number</th>
            <th>Email</th>
            <th>City</th>
            <th>Zip</th>
            <th>Street and number</th>
            <th>Country</th>
            <th>Edit</th>
            <th>Delete</th>
        </tr>
        </thead>
        <tbody>
        ";
        // line 30
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["contacts"]) || array_key_exists("contacts", $context) ? $context["contacts"] : (function () { throw new Twig_Error_Runtime('Variable "contacts" does not exist.', 30, $this->source); })()));
        foreach ($context['_seq'] as $context["_key"] => $context["contact"]) {
            // line 31
            echo "            <tr>
                <td><img src=\"";
            // line 32
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["contact"], "picture", []), "html", null, true);
            echo "\"></td>
                <td>";
            // line 33
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["contact"], "firstName", []), "html", null, true);
            echo "</td>
                <td>";
            // line 34
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["contact"], "lastName", []), "html", null, true);
            echo "</td>
                <td>";
            // line 35
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["contact"], "birthday", []), "d.m.Y"), "html", null, true);
            echo "</td>
                <td>";
            // line 36
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["contact"], "phoneNumber", []), "html", null, true);
            echo "</td>
                <td>";
            // line 37
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["contact"], "email", []), "html", null, true);
            echo "</td>
                <td>";
            // line 38
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["contact"], "city", []), "html", null, true);
            echo "</td>
                <td>";
            // line 39
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["contact"], "zip", []), "html", null, true);
            echo "</td>
                <td>";
            // line 40
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["contact"], "address", []), "html", null, true);
            echo "</td>
                <td>";
            // line 41
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["contact"], "country", []), "html", null, true);
            echo "</td>
                <td>
                    <a href=\"/contact/";
            // line 43
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["contact"], "id", []), "html", null, true);
            echo "/edit\">
                        <button>
                            <ion-icon name=\"create\"></ion-icon>
                        </button>
                    </a>
                </td>
                <td>
                    <!-- I tried to set method to DELETE and route /contact/id, but unsuccessfully, so I decided for post just to finish the task  -->
                    <form method=\"POST\" action=\"/contact/";
            // line 51
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["contact"], "id", []), "html", null, true);
            echo "/delete\">
                        <button type=\"submit\" class=\"delete-btn\">
                            <ion-icon name=\"trash\"></ion-icon>
                        </button>
                    </form>
                </td>
            </tr>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['contact'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 59
        echo "        </tbody>
    </table>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "contact/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  154 => 59,  140 => 51,  129 => 43,  124 => 41,  120 => 40,  116 => 39,  112 => 38,  108 => 37,  104 => 36,  100 => 35,  96 => 34,  92 => 33,  88 => 32,  85 => 31,  81 => 30,  53 => 4,  44 => 3,  15 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block body %}
    <a href=\"/contact/create\">
        <button class=\"add-new-btn\">Add new
        </button>
    </a>
    <form method=\"GET\" action=\"/\">
        <input type=\"text\" name=\"term\">
        <input type=\"submit\" value=\"Search\">
    </form>
    <table class=\"contact-tb\">
        <thead>
        <tr>
            <th>Picture</th>
            <th>First Name</th>
            <th>Last Name</th>
            <th>Birthday</th>
            <th>Phone number</th>
            <th>Email</th>
            <th>City</th>
            <th>Zip</th>
            <th>Street and number</th>
            <th>Country</th>
            <th>Edit</th>
            <th>Delete</th>
        </tr>
        </thead>
        <tbody>
        {% for contact in contacts %}
            <tr>
                <td><img src=\"{{ contact.picture }}\"></td>
                <td>{{ contact.firstName }}</td>
                <td>{{ contact.lastName }}</td>
                <td>{{ contact.birthday|date('d.m.Y') }}</td>
                <td>{{ contact.phoneNumber }}</td>
                <td>{{ contact.email }}</td>
                <td>{{ contact.city }}</td>
                <td>{{ contact.zip }}</td>
                <td>{{ contact.address }}</td>
                <td>{{ contact.country }}</td>
                <td>
                    <a href=\"/contact/{{ contact.id }}/edit\">
                        <button>
                            <ion-icon name=\"create\"></ion-icon>
                        </button>
                    </a>
                </td>
                <td>
                    <!-- I tried to set method to DELETE and route /contact/id, but unsuccessfully, so I decided for post just to finish the task  -->
                    <form method=\"POST\" action=\"/contact/{{ contact.id }}/delete\">
                        <button type=\"submit\" class=\"delete-btn\">
                            <ion-icon name=\"trash\"></ion-icon>
                        </button>
                    </form>
                </td>
            </tr>
        {% endfor %}
        </tbody>
    </table>
{% endblock %}", "contact/index.html.twig", "C:\\Users\\Filip\\Documents\\dev\\AddressBook\\templates\\contact\\index.html.twig");
    }
}
