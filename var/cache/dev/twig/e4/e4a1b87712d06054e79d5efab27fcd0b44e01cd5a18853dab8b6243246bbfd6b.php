<?php

/* base.html.twig */
class __TwigTemplate_ea9b6224e77393e98e4728bfb19099483a2e3d76834bdd70496a870d4ea2897c extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "base.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "base.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        <title>Address Book</title>
        <style type=\"text/css\">
            .content {
                padding: 10px;
            }

            .delete-btn {
                cursor: pointer;
                color: red;
            }

            .contact-tb {
                font-family: \"Trebuchet MS\", Arial, Helvetica, sans-serif;
                border-collapse: collapse;
                width: 100%;
                margin-top: 20px;
            }

            .contact-tb td, .contact-tb th {
                border: 1px solid #ddd;
                padding: 8px;
            }

            .contact-tb tr:nth-child(even) {
                background-color: #f2f2f2;
            }

            .contact-tb tr:hover {
                background-color: #ddd;
            }

            .contact-tb th {
                padding-top: 12px;
                padding-bottom: 12px;
                text-align: left;
                background-color: #00afab;
                color: white;
            }

            input[type=text], [type=email], [type=date], select {
                padding: 12px 20px;
                margin: 8px 0;
                display: inline-block;
                border: 1px solid #ccc;
                border-radius: 4px;
                box-sizing: border-box;
                font-size: 16px;
            }

            input[type=submit], .add-new-btn {
                background-color: #00afab;
                color: white;
                padding: 14px 20px;
                margin: 8px 0;
                border: none;
                cursor: pointer;
                border-color: #99dfdd;
            }

            input[type=submit]:hover, .add-new-btn:hover {
                background-color: #99dfdd;
            }

            .add-new-btn {
                float: right;
            }

            .add-new-btn a {
                text-decoration: none;
                color: white;
            }
        </style>
    </head>
    <body>
        <div class=\"content\">
            ";
        // line 79
        $this->displayBlock('body', $context, $blocks);
        // line 80
        echo "        </div>
    <script src=\"https://unpkg.com/ionicons@4.5.5/dist/ionicons.js\"></script>
    </body>
</html>";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 79
    public function block_body($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "base.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  125 => 79,  112 => 80,  110 => 79,  30 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!DOCTYPE html>
<html>
    <head>
        <title>Address Book</title>
        <style type=\"text/css\">
            .content {
                padding: 10px;
            }

            .delete-btn {
                cursor: pointer;
                color: red;
            }

            .contact-tb {
                font-family: \"Trebuchet MS\", Arial, Helvetica, sans-serif;
                border-collapse: collapse;
                width: 100%;
                margin-top: 20px;
            }

            .contact-tb td, .contact-tb th {
                border: 1px solid #ddd;
                padding: 8px;
            }

            .contact-tb tr:nth-child(even) {
                background-color: #f2f2f2;
            }

            .contact-tb tr:hover {
                background-color: #ddd;
            }

            .contact-tb th {
                padding-top: 12px;
                padding-bottom: 12px;
                text-align: left;
                background-color: #00afab;
                color: white;
            }

            input[type=text], [type=email], [type=date], select {
                padding: 12px 20px;
                margin: 8px 0;
                display: inline-block;
                border: 1px solid #ccc;
                border-radius: 4px;
                box-sizing: border-box;
                font-size: 16px;
            }

            input[type=submit], .add-new-btn {
                background-color: #00afab;
                color: white;
                padding: 14px 20px;
                margin: 8px 0;
                border: none;
                cursor: pointer;
                border-color: #99dfdd;
            }

            input[type=submit]:hover, .add-new-btn:hover {
                background-color: #99dfdd;
            }

            .add-new-btn {
                float: right;
            }

            .add-new-btn a {
                text-decoration: none;
                color: white;
            }
        </style>
    </head>
    <body>
        <div class=\"content\">
            {% block body %}{% endblock %}
        </div>
    <script src=\"https://unpkg.com/ionicons@4.5.5/dist/ionicons.js\"></script>
    </body>
</html>", "base.html.twig", "C:\\Users\\Filip\\Documents\\dev\\AddressBook\\templates\\base.html.twig");
    }
}
