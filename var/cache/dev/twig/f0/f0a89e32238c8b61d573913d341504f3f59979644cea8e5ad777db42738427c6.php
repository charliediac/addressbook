<?php

/* contact/create.html.twig */
class __TwigTemplate_d1a23d84695768928f475cf10d4f8eda6bc2e9b1bba2d553c3106c008135ddab extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "contact/create.html.twig", 1);
        $this->blocks = [
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "contact/create.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "contact/create.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_body($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "    <div class=\"nav\">
        <a href=\"/\">
            <button class=\"add-new-btn\">
                Contacts
            </button>
        </a>
    </div>
    <form method=\"POST\" action=\"/contact\">
        <div class=\"form-block\">
            <label for=\"firstName\">First Name</label><br/>
            <input type=\"text\" name=\"firstName\">
        </div>
        <div class=\"form-block\">
            <label for=\"lastName\">Last Name</label><br/>
            <input type=\"text\" name=\"lastName\">
        </div>
        <div class=\"form-block\">
            <label for=\"birthday\">Birthday</label><br/>
            <input type=\"date\" name=\"birthday\">
        </div>
        <div class=\"form-block\">
            <label for=\"phoneNumber\">Phone number</label><br/>
            <input type=\"text\" name=\"phoneNumber\">
        </div>
        <div class=\"form-block\">
            <label for=\"email\">Email</label><br/>
            <input type=\"email\" name=\"email\">
        </div>
        <div class=\"form-block\">
            <label for=\"city\">City</label><br/>
            <input type=\"text\" name=\"city\">
        </div>
        <div class=\"form-block\">
            <label for=\"zip\">ZIP</label><br/>
            <input type=\"text\" name=\"zip\">
        </div>
        <div class=\"form-block\">
            <label for=\"address\">Address</label><br/>
            <input type=\"text\" name=\"address\">
        </div>
        <div class=\"form-block\">
            <label for=\"country\">Country</label><br/>
            <input type=\"text\" name=\"country\">
        </div>
        <div class=\"form-block\">
            <label for=\"picture\">Picture</label><br/>
            <input type=\"file\" name=\"picture\">
        </div>

        <input type=\"submit\" value=\"Save\">
    </form>

    ";
        // line 56
        if ((isset($context["errors"]) || array_key_exists("errors", $context))) {
            // line 57
            echo "        <div>
            <h3>Errors:</h3>
            <ul>
                ";
            // line 60
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["errors"]) || array_key_exists("errors", $context) ? $context["errors"] : (function () { throw new Twig_Error_Runtime('Variable "errors" does not exist.', 60, $this->source); })()));
            foreach ($context['_seq'] as $context["_key"] => $context["error"]) {
                // line 61
                echo "                    <li> ";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["error"], "propertyPath", []), "html", null, true);
                echo " -> ";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["error"], "message", []), "html", null, true);
                echo "</li>
                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['error'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 63
            echo "            </ul>
        </div>
    ";
        }
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "contact/create.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  129 => 63,  118 => 61,  114 => 60,  109 => 57,  107 => 56,  53 => 4,  44 => 3,  15 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block body %}
    <div class=\"nav\">
        <a href=\"/\">
            <button class=\"add-new-btn\">
                Contacts
            </button>
        </a>
    </div>
    <form method=\"POST\" action=\"/contact\">
        <div class=\"form-block\">
            <label for=\"firstName\">First Name</label><br/>
            <input type=\"text\" name=\"firstName\">
        </div>
        <div class=\"form-block\">
            <label for=\"lastName\">Last Name</label><br/>
            <input type=\"text\" name=\"lastName\">
        </div>
        <div class=\"form-block\">
            <label for=\"birthday\">Birthday</label><br/>
            <input type=\"date\" name=\"birthday\">
        </div>
        <div class=\"form-block\">
            <label for=\"phoneNumber\">Phone number</label><br/>
            <input type=\"text\" name=\"phoneNumber\">
        </div>
        <div class=\"form-block\">
            <label for=\"email\">Email</label><br/>
            <input type=\"email\" name=\"email\">
        </div>
        <div class=\"form-block\">
            <label for=\"city\">City</label><br/>
            <input type=\"text\" name=\"city\">
        </div>
        <div class=\"form-block\">
            <label for=\"zip\">ZIP</label><br/>
            <input type=\"text\" name=\"zip\">
        </div>
        <div class=\"form-block\">
            <label for=\"address\">Address</label><br/>
            <input type=\"text\" name=\"address\">
        </div>
        <div class=\"form-block\">
            <label for=\"country\">Country</label><br/>
            <input type=\"text\" name=\"country\">
        </div>
        <div class=\"form-block\">
            <label for=\"picture\">Picture</label><br/>
            <input type=\"file\" name=\"picture\">
        </div>

        <input type=\"submit\" value=\"Save\">
    </form>

    {% if errors is defined %}
        <div>
            <h3>Errors:</h3>
            <ul>
                {% for error in errors %}
                    <li> {{ error.propertyPath }} -> {{ error.message }}</li>
                {% endfor %}
            </ul>
        </div>
    {% endif %}
{% endblock %}", "contact/create.html.twig", "C:\\Users\\Filip\\Documents\\dev\\AddressBook\\templates\\contact\\create.html.twig");
    }
}
