<?php

/* contact/edit.html.twig */
class __TwigTemplate_e1ef721e0cedb5ab119cae6051d3805e07307084a466dfc6aac788f39ab2036d extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "contact/edit.html.twig", 1);
        $this->blocks = [
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "contact/edit.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "contact/edit.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_body($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "    <div class=\"nav\">
        <a href=\"/\">
            <button class=\"add-new-btn\">
                Contacts
            </button>
        </a>
    </div>
    <!-- I tried to set method to PUT and route /contact/id, but unsuccessfully, so I decided for post just to finish the task  -->
    <form method=\"POST\" action=\"/contact/";
        // line 12
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["contact"]) || array_key_exists("contact", $context) ? $context["contact"] : (function () { throw new Twig_Error_Runtime('Variable "contact" does not exist.', 12, $this->source); })()), "id", []), "html", null, true);
        echo "\">
        <div class=\"form-block\">
            <label for=\"firstName\">First Name</label><br/>
            <input type=\"text\" name=\"firstName\" value=\"";
        // line 15
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["contact"]) || array_key_exists("contact", $context) ? $context["contact"] : (function () { throw new Twig_Error_Runtime('Variable "contact" does not exist.', 15, $this->source); })()), "firstName", []), "html", null, true);
        echo "\">
        </div>
        <div class=\"form-block\">
            <label for=\"lastName\">Last Name</label><br/>
            <input type=\"text\" name=\"lastName\" value=\"";
        // line 19
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["contact"]) || array_key_exists("contact", $context) ? $context["contact"] : (function () { throw new Twig_Error_Runtime('Variable "contact" does not exist.', 19, $this->source); })()), "lastName", []), "html", null, true);
        echo "\">
        </div>
        <div class=\"form-block\">
            <label for=\"birthday\">Birthdat</label><br/>
            <input type=\"text\" name=\"birthday\" value=\"";
        // line 23
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["contact"]) || array_key_exists("contact", $context) ? $context["contact"] : (function () { throw new Twig_Error_Runtime('Variable "contact" does not exist.', 23, $this->source); })()), "birthday", []), "d.m.Y"), "html", null, true);
        echo "\">
        </div>
        <div class=\"form-block\">
            <label for=\"city\">City</label><br/>
            <input type=\"text\" name=\"city\" value=\"";
        // line 27
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["contact"]) || array_key_exists("contact", $context) ? $context["contact"] : (function () { throw new Twig_Error_Runtime('Variable "contact" does not exist.', 27, $this->source); })()), "city", []), "html", null, true);
        echo "\">
        </div>
        <div class=\"form-block\">
            <label for=\"zip\">ZIP</label><br/>
            <input type=\"text\" name=\"zip\" value=\"";
        // line 31
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["contact"]) || array_key_exists("contact", $context) ? $context["contact"] : (function () { throw new Twig_Error_Runtime('Variable "contact" does not exist.', 31, $this->source); })()), "zip", []), "html", null, true);
        echo "\">
        </div>
        <div class=\"form-block\">
            <label for=\"address\">Address</label><br/>
            <input type=\"text\" name=\"address\" value=\"";
        // line 35
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["contact"]) || array_key_exists("contact", $context) ? $context["contact"] : (function () { throw new Twig_Error_Runtime('Variable "contact" does not exist.', 35, $this->source); })()), "address", []), "html", null, true);
        echo "\">
        </div>
        <div class=\"form-block\">
            <label for=\"country\">Country</label><br/>
            <input type=\"text\" name=\"country\" value=\"";
        // line 39
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["contact"]) || array_key_exists("contact", $context) ? $context["contact"] : (function () { throw new Twig_Error_Runtime('Variable "contact" does not exist.', 39, $this->source); })()), "country", []), "html", null, true);
        echo "\">
        </div>
        <div class=\"form-block\">
            <label for=\"phoneNumber\">Phone number</label><br/>
            <input type=\"text\" name=\"phoneNumber\" value=\"";
        // line 43
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["contact"]) || array_key_exists("contact", $context) ? $context["contact"] : (function () { throw new Twig_Error_Runtime('Variable "contact" does not exist.', 43, $this->source); })()), "phoneNumber", []), "html", null, true);
        echo "\">
        </div>
        <div class=\"form-block\">
            <label for=\"email\">Email</label><br/>
            <input type=\"email\" name=\"email\" value=\"";
        // line 47
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["contact"]) || array_key_exists("contact", $context) ? $context["contact"] : (function () { throw new Twig_Error_Runtime('Variable "contact" does not exist.', 47, $this->source); })()), "email", []), "html", null, true);
        echo "\">
        </div>
        <div class=\"form-block\">
            <label for=\"picture\">Image</label><br/>
            <input type=\"text\" name=\"picture\" value=\"";
        // line 51
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["contact"]) || array_key_exists("contact", $context) ? $context["contact"] : (function () { throw new Twig_Error_Runtime('Variable "contact" does not exist.', 51, $this->source); })()), "picture", []), "html", null, true);
        echo "\">
        </div>

        <input type=\"submit\" value=\"Update\">
    </form>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "contact/edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  132 => 51,  125 => 47,  118 => 43,  111 => 39,  104 => 35,  97 => 31,  90 => 27,  83 => 23,  76 => 19,  69 => 15,  63 => 12,  53 => 4,  44 => 3,  15 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block body %}
    <div class=\"nav\">
        <a href=\"/\">
            <button class=\"add-new-btn\">
                Contacts
            </button>
        </a>
    </div>
    <!-- I tried to set method to PUT and route /contact/id, but unsuccessfully, so I decided for post just to finish the task  -->
    <form method=\"POST\" action=\"/contact/{{ contact.id }}\">
        <div class=\"form-block\">
            <label for=\"firstName\">First Name</label><br/>
            <input type=\"text\" name=\"firstName\" value=\"{{ contact.firstName }}\">
        </div>
        <div class=\"form-block\">
            <label for=\"lastName\">Last Name</label><br/>
            <input type=\"text\" name=\"lastName\" value=\"{{ contact.lastName }}\">
        </div>
        <div class=\"form-block\">
            <label for=\"birthday\">Birthdat</label><br/>
            <input type=\"text\" name=\"birthday\" value=\"{{ contact.birthday|date(\"d.m.Y\") }}\">
        </div>
        <div class=\"form-block\">
            <label for=\"city\">City</label><br/>
            <input type=\"text\" name=\"city\" value=\"{{ contact.city }}\">
        </div>
        <div class=\"form-block\">
            <label for=\"zip\">ZIP</label><br/>
            <input type=\"text\" name=\"zip\" value=\"{{ contact.zip }}\">
        </div>
        <div class=\"form-block\">
            <label for=\"address\">Address</label><br/>
            <input type=\"text\" name=\"address\" value=\"{{ contact.address }}\">
        </div>
        <div class=\"form-block\">
            <label for=\"country\">Country</label><br/>
            <input type=\"text\" name=\"country\" value=\"{{ contact.country }}\">
        </div>
        <div class=\"form-block\">
            <label for=\"phoneNumber\">Phone number</label><br/>
            <input type=\"text\" name=\"phoneNumber\" value=\"{{ contact.phoneNumber }}\">
        </div>
        <div class=\"form-block\">
            <label for=\"email\">Email</label><br/>
            <input type=\"email\" name=\"email\" value=\"{{ contact.email }}\">
        </div>
        <div class=\"form-block\">
            <label for=\"picture\">Image</label><br/>
            <input type=\"text\" name=\"picture\" value=\"{{ contact.picture}}\">
        </div>

        <input type=\"submit\" value=\"Update\">
    </form>
{% endblock %}", "contact/edit.html.twig", "C:\\Users\\Filip\\Documents\\dev\\AddressBook\\templates\\contact\\edit.html.twig");
    }
}
