<?php

/* contact/edit.html.twig */
class __TwigTemplate_2a91016be4d1d4cf3c2580869043a8a13e90c7b9bf4cf809ae73fee1882ffebd extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "contact/edit.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "contact/edit.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html>
<head>
    <title>Address Book</title>
    <style type=\"text/css\">

        .content {
            padding: 10px;
        }
        .delete-btn {
            cursor: pointer;
            color: red;
        }
        .contact-tb {
            font-family: \"Trebuchet MS\", Arial, Helvetica, sans-serif;
            border-collapse: collapse;
            width: 100%;
            margin-top:20px;
        }
        .contact-tb td, .contact-tb th {
            border: 1px solid #ddd;
            padding: 8px;
        }

        .contact-tb tr:nth-child(even){background-color: #f2f2f2;}

        .contact-tb tr:hover {background-color: #ddd;}

        .contact-tb th {
            padding-top: 12px;
            padding-bottom: 12px;
            text-align: left;
            background-color: #00afab;
            color: white;
        }
        input[type=text], input[type=email], select {
            padding: 12px 20px;
            margin: 8px 0;
            display: inline-block;
            border: 1px solid #ccc;
            border-radius: 4px;
            box-sizing: border-box;
            font-size:16px;
        }
        input[type=submit], .add-new-btn {
            background-color: #00afab;
            color: white;
            padding: 14px 20px;
            margin: 8px 0;
            border: none;
            cursor: pointer;
            border-color: #99dfdd;
        }
        input[type=submit]:hover, .add-new-btn:hover {
            background-color: #99dfdd;
        }

        .add-new-btn a{
            text-decoration: none;
            color:white;
        }
    </style>
</head>
<body>
<div class=\"content\">
    <div class=\"nav\">
        <button class=\"add-new-btn\">
            <a href=\"/contact\">Contacts</a>
        </button>
    </div>
    <form method=\"PUT\" action=\"/contact/";
        // line 71
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["contact"]) || array_key_exists("contact", $context) ? $context["contact"] : (function () { throw new Twig_Error_Runtime('Variable "contact" does not exist.', 71, $this->source); })()), "id", []), "html", null, true);
        echo "\">
        <div class=\"form-block\">
            <label for=\"firstName\">First Name</label><br/>
            <input type=\"text\" name=\"firstName\" value=\"";
        // line 74
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["contact"]) || array_key_exists("contact", $context) ? $context["contact"] : (function () { throw new Twig_Error_Runtime('Variable "contact" does not exist.', 74, $this->source); })()), "firstName", []), "html", null, true);
        echo "\">
        </div>
        <div class=\"form-block\">
            <label for=\"lastName\">Last Name</label><br/>
            <input type=\"text\" name=\"lastName\" value=\"";
        // line 78
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["contact"]) || array_key_exists("contact", $context) ? $context["contact"] : (function () { throw new Twig_Error_Runtime('Variable "contact" does not exist.', 78, $this->source); })()), "lastName", []), "html", null, true);
        echo "\">
        </div>
        <div class=\"form-block\">
            <label for=\"birthday\">Birthdat</label><br/>
            <input type=\"date\" name=\"birthday\" value=\"";
        // line 82
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["contact"]) || array_key_exists("contact", $context) ? $context["contact"] : (function () { throw new Twig_Error_Runtime('Variable "contact" does not exist.', 82, $this->source); })()), "birthday", []), "html", null, true);
        echo "\">
        </div>
        <div class=\"form-block\">
            <label for=\"city\">City</label><br/>
            <input type=\"text\" name=\"city\" value=\"";
        // line 86
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["contact"]) || array_key_exists("contact", $context) ? $context["contact"] : (function () { throw new Twig_Error_Runtime('Variable "contact" does not exist.', 86, $this->source); })()), "city", []), "html", null, true);
        echo "\">
        </div>
        <div class=\"form-block\">
            <label for=\"zip\">ZIP</label><br/>
            <input type=\"text\" name=\"zip\" value=\"";
        // line 90
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["contact"]) || array_key_exists("contact", $context) ? $context["contact"] : (function () { throw new Twig_Error_Runtime('Variable "contact" does not exist.', 90, $this->source); })()), "zip", []), "html", null, true);
        echo "\">
        </div>
        <div class=\"form-block\">
            <label for=\"address\">Address</label><br/>
            <input type=\"text\" name=\"address\" value=\"";
        // line 94
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["contact"]) || array_key_exists("contact", $context) ? $context["contact"] : (function () { throw new Twig_Error_Runtime('Variable "contact" does not exist.', 94, $this->source); })()), "address", []), "html", null, true);
        echo "\">
        </div>
        <div class=\"form-block\">
            <label for=\"country\">Country</label><br/>
            <input type=\"text\" name=\"country\" value=\"";
        // line 98
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["contact"]) || array_key_exists("contact", $context) ? $context["contact"] : (function () { throw new Twig_Error_Runtime('Variable "contact" does not exist.', 98, $this->source); })()), "country", []), "html", null, true);
        echo "\">
        </div>
        <div class=\"form-block\">
            <label for=\"phoneNumber\">Phone number</label><br/>
            <input type=\"text\" name=\"phoneNumber\" value=\"";
        // line 102
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["contact"]) || array_key_exists("contact", $context) ? $context["contact"] : (function () { throw new Twig_Error_Runtime('Variable "contact" does not exist.', 102, $this->source); })()), "phoneNumber", []), "html", null, true);
        echo "\">
        </div>
        <div class=\"form-block\">
            <label for=\"email\">Email</label><br/>
            <input type=\"email\" name=\"email\" value=\"";
        // line 106
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["contact"]) || array_key_exists("contact", $context) ? $context["contact"] : (function () { throw new Twig_Error_Runtime('Variable "contact" does not exist.', 106, $this->source); })()), "email", []), "html", null, true);
        echo "\">
        </div>
        <div class=\"form-block\">
            <label for=\"image\">Image</label><br/>
            <input type=\"text\" name=\"image\" value=\"";
        // line 110
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["contact"]) || array_key_exists("contact", $context) ? $context["contact"] : (function () { throw new Twig_Error_Runtime('Variable "contact" does not exist.', 110, $this->source); })()), "image", []), "html", null, true);
        echo "\">
        </div>

        <input type=\"submit\" value=\"Update\">
    </form>
</div>
</body>
</html>";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "contact/edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  170 => 110,  163 => 106,  156 => 102,  149 => 98,  142 => 94,  135 => 90,  128 => 86,  121 => 82,  114 => 78,  107 => 74,  101 => 71,  29 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!DOCTYPE html>
<html>
<head>
    <title>Address Book</title>
    <style type=\"text/css\">

        .content {
            padding: 10px;
        }
        .delete-btn {
            cursor: pointer;
            color: red;
        }
        .contact-tb {
            font-family: \"Trebuchet MS\", Arial, Helvetica, sans-serif;
            border-collapse: collapse;
            width: 100%;
            margin-top:20px;
        }
        .contact-tb td, .contact-tb th {
            border: 1px solid #ddd;
            padding: 8px;
        }

        .contact-tb tr:nth-child(even){background-color: #f2f2f2;}

        .contact-tb tr:hover {background-color: #ddd;}

        .contact-tb th {
            padding-top: 12px;
            padding-bottom: 12px;
            text-align: left;
            background-color: #00afab;
            color: white;
        }
        input[type=text], input[type=email], select {
            padding: 12px 20px;
            margin: 8px 0;
            display: inline-block;
            border: 1px solid #ccc;
            border-radius: 4px;
            box-sizing: border-box;
            font-size:16px;
        }
        input[type=submit], .add-new-btn {
            background-color: #00afab;
            color: white;
            padding: 14px 20px;
            margin: 8px 0;
            border: none;
            cursor: pointer;
            border-color: #99dfdd;
        }
        input[type=submit]:hover, .add-new-btn:hover {
            background-color: #99dfdd;
        }

        .add-new-btn a{
            text-decoration: none;
            color:white;
        }
    </style>
</head>
<body>
<div class=\"content\">
    <div class=\"nav\">
        <button class=\"add-new-btn\">
            <a href=\"/contact\">Contacts</a>
        </button>
    </div>
    <form method=\"PUT\" action=\"/contact/{{ contact.id }}\">
        <div class=\"form-block\">
            <label for=\"firstName\">First Name</label><br/>
            <input type=\"text\" name=\"firstName\" value=\"{{ contact.firstName }}\">
        </div>
        <div class=\"form-block\">
            <label for=\"lastName\">Last Name</label><br/>
            <input type=\"text\" name=\"lastName\" value=\"{{ contact.lastName }}\">
        </div>
        <div class=\"form-block\">
            <label for=\"birthday\">Birthdat</label><br/>
            <input type=\"date\" name=\"birthday\" value=\"{{ contact.birthday }}\">
        </div>
        <div class=\"form-block\">
            <label for=\"city\">City</label><br/>
            <input type=\"text\" name=\"city\" value=\"{{ contact.city }}\">
        </div>
        <div class=\"form-block\">
            <label for=\"zip\">ZIP</label><br/>
            <input type=\"text\" name=\"zip\" value=\"{{ contact.zip }}\">
        </div>
        <div class=\"form-block\">
            <label for=\"address\">Address</label><br/>
            <input type=\"text\" name=\"address\" value=\"{{ contact.address }}\">
        </div>
        <div class=\"form-block\">
            <label for=\"country\">Country</label><br/>
            <input type=\"text\" name=\"country\" value=\"{{ contact.country }}\">
        </div>
        <div class=\"form-block\">
            <label for=\"phoneNumber\">Phone number</label><br/>
            <input type=\"text\" name=\"phoneNumber\" value=\"{{ contact.phoneNumber }}\">
        </div>
        <div class=\"form-block\">
            <label for=\"email\">Email</label><br/>
            <input type=\"email\" name=\"email\" value=\"{{ contact.email }}\">
        </div>
        <div class=\"form-block\">
            <label for=\"image\">Image</label><br/>
            <input type=\"text\" name=\"image\" value=\"{{ contact.image }}\">
        </div>

        <input type=\"submit\" value=\"Update\">
    </form>
</div>
</body>
</html>", "contact/edit.html.twig", "C:\\Users\\Filip\\Documents\\dev\\AddressBook\\templates\\contact\\edit.html.twig");
    }
}
