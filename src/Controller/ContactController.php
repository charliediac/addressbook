<?php
/**
 * Created by PhpStorm.
 * User: Filip
 * Date: 11/02/2019
 * Time: 13:01
 */

namespace App\Controller;

use App\Entity\Contact;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * Class ContactController
 * @package App\Controller
 */
class ContactController extends Controller
{
    /**
     * @Route("/", methods={"GET"})
     */
    public function indexAction()
    {
        $request = Request::createFromGlobals();
        $term = $request->get('term');
        if ($term) {
            $term = '%' . $term . '%';
            $em = $this->getDoctrine()->getManager();
            $query = $em->createQuery("SELECT c FROM App\Entity\Contact c WHERE c.firstName LIKE '" . $term . "' OR c.lastName LIKE '" . $term . "' OR c.phoneNumber LIKE '" . $term . "'");
            $contacts = $query->getResult();
        } else {
            $contacts = $this->getDoctrine()->getRepository('App:Contact')->findAll();
        }

        return $this->render('contact/index.html.twig', [
            'contacts' => $contacts
        ]);
    }

    /**
     * @Route("/contact/create", methods={"GET"})
     */
    public function newAction()
    {
        return $this->render('contact/create.html.twig');
    }

    /**
     * @Route("/contact/{contact}/edit", methods={"GET"})
     */
    public function editAction(Contact $contact)
    {
        return $this->render('contact/edit.html.twig', ['contact' => $contact]);

    }

    /**
     * @Route("/contact", methods={"POST"})
     */
    public function createAction(ValidatorInterface $validator)
    {
        $request = Request::createFromGlobals();

        $contact = new Contact();
        $contact->setFirstName($request->get('firstName'));
        $contact->setLastName($request->get('lastName'));
        $contact->setBirthday(new \DateTime($request->get('birthday')));
        $contact->setPhoneNumber($request->get('phoneNumber'));
        $contact->setEmail($request->get('email'));
        $contact->setCity($request->get('city'));
        $contact->setZip($request->get('zip'));
        $contact->setAddress($request->get('address'));
        $contact->setCountry($request->get('country'));
        $contact->setPicture($request->get('picture'));

        $em = $this->getDoctrine()->getManager();
        $em->persist($contact);
        $em->flush();

        return $this->indexAction();
    }

    /**
     * @Route("/contact/{contact}", methods={"POST"})
     */
    public function updateAction(Contact $contact)
    {
        $request = Request::createFromGlobals();

        $contact->setFirstName($request->get('firstName'));
        $contact->setLastName($request->get('lastName'));
        $contact->setBirthday(new \DateTime($request->get('birthday')));
        $contact->setPhoneNumber($request->get('phoneNumber'));
        $contact->setEmail($request->get('email'));
        $contact->setCity($request->get('city'));
        $contact->setZip($request->get('zip'));
        $contact->setAddress($request->get('address'));
        $contact->setCountry($request->get('country'));
        $contact->setPicture($request->get('picture'));

        $em = $this->getDoctrine()->getManager();
        $em->persist($contact);
        $em->flush();

        return $this->render('contact/edit.html.twig', ['contact' => $contact]);
    }

    /**
     * @Route("/contact/{contact}/delete", methods={"POST"})
     */
    public function deleteAction(Contact $contact)
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($contact);
        $em->flush();

        return $this->indexAction();
    }

}